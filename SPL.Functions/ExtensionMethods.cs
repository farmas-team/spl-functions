﻿using LTAF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SPL.Functions
{
    public static class ExtensionMethods
    {
        public static ReadOnlyCollection<HtmlElement> FindAllByClass(this HtmlElementCollection root, string className)
        {
            var findParams = new HtmlElementFindParams();
            findParams.Attributes.Add("class", $"(^|\\s){className}($|\\s)", MatchMethod.Regex);
            return root.FindAll(findParams);
        }

        public static HtmlElement FindByClass(this HtmlElementCollection root, string className)
        {
            var findParams = new HtmlElementFindParams();
            findParams.Attributes.Add("class", $"(^|\\s){className}($|\\s)", MatchMethod.Regex);
            return root.Find(findParams);
        }

        public static bool TryFindByClass(this HtmlElementCollection root, string className, out HtmlElement element)
        {
            var findParams = new HtmlElementFindParams();
            findParams.Attributes.Add("class", $"(^|\\s){className}($|\\s)", MatchMethod.Regex);

            try
            {
                element = root.Find(findParams);
                return true;
            }
            catch (ElementNotFoundException)
            {
                element = null;
                return false;
            }
        }
    }
}
