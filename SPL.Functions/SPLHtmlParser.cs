﻿using LTAF;
using Newtonsoft.Json.Linq;
using SPL.Functions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SPL.Functions
{
    public class SPLHtmlParser
    {
        private static JObject GetJsonData(HtmlElement root)
        {
            var findParams = new HtmlElementFindParams();
            findParams.TagName = "script";
            findParams.Attributes.Add("data-iso-key", "_0");
            var script = root.ChildElements.Find(findParams).CachedInnerTextRecursive;
            return JObject.Parse(script);
        }

        public static List<HeldItem> GetHeldItems(HtmlElement root, out HeldSummary summary)
        {
            var json = GetJsonData(root);
            var entities = (JObject)json["entities"];
            var holdSummary = (JObject)json["borrowing"]["holds"]["summary"];
            var holds = (JObject)entities["holds"];
            var bibs = (JObject)entities["bibs"];

            var readyItems = new List<HeldItem>();
            var inTransitItems = new List<HeldItem>();
            var pendingItems = new List<HeldItem>();

            foreach (var prop in holds.Properties())
            {
                var jsonItem = (JObject)holds[prop.Name];
                var item = new HeldItem()
                {
                    MetadataId = jsonItem["metadataId"].ToString(),
                    Title = jsonItem["bibTitle"].ToString(),
                    Position = (int)jsonItem["holdsPosition"],
                    IsReady = String.Equals(jsonItem["status"].ToString(), "READY_FOR_PICKUP", StringComparison.OrdinalIgnoreCase),
                    IsInTransit = String.Equals(jsonItem["status"].ToString(), "IN_TRANSIT", StringComparison.OrdinalIgnoreCase)
                };

                item.ImageSrc = bibs[item.MetadataId]["briefInfo"]["jacket"]["small"].ToString();

                if (item.IsReady)
                {
                    readyItems.Add(item);
                }
                else if (item.IsInTransit)
                {
                    inTransitItems.Add(item);
                }
                else
                {
                    pendingItems.Add(item);
                }
            }

            summary = new HeldSummary()
            {
                Total = (int)holdSummary["totalOperative"],
                AnyReady = readyItems.Any()
            };

            var result = new List<HeldItem>(readyItems);
            result.AddRange(inTransitItems);
            result.AddRange(pendingItems.OrderBy(i => i.Position));
            return result;
        }

        public static List<CheckedOutItem> GetCheckedOutItems(HtmlElement root, out CheckedOutSummary summary)
        {
            var json = GetJsonData(root);
            var entities = (JObject)json["entities"];
            var checkouts = (JObject)entities["checkouts"];
            var bibs = (JObject)entities["bibs"];

            summary = new CheckedOutSummary();
            var items = new List<CheckedOutItem>();
            var oldest = "3000-01-01";
            foreach (var prop in checkouts.Properties())
            {
                var jsonItem = (JObject)checkouts[prop.Name];
                var item = new CheckedOutItem()
                {
                    MetadataId = jsonItem["metadataId"].ToString(),
                    Barcode = jsonItem["barcode"].ToString(),
                    Title = jsonItem["bibTitle"].ToString(),
                    DueDate = jsonItem["dueDate"].ToString(),
                    IsExpired = String.Equals(jsonItem["status"].ToString(), "OVERDUE", StringComparison.OrdinalIgnoreCase)

                };

                item.ImageSrc = bibs[item.MetadataId]["briefInfo"]["jacket"]["small"].ToString();

                summary.IsExpired = summary.IsExpired || item.IsExpired;
                if (String.Compare(item.DueDate, oldest, StringComparison.OrdinalIgnoreCase) < 0)
                {
                    oldest = item.DueDate;
                }

                items.Add(item);
            }

            if (items.Count > 0)
            {
                summary.NextDueDate = oldest;
            }

            return items;
        }
    }
}
