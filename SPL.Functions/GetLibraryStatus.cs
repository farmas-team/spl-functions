using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Text;
using LTAF;
using Microsoft.Extensions.Configuration;
using SPL.Functions.Models;

namespace SPL.Functions
{
    public static class GetLibraryStatus
    {
        [FunctionName("GetLibraryStatus")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var config = GetConfiguration(context);
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string username = ((string)data?.splUsername) ?? config["SPL_USER"];
            string password = ((string)data?.splPassword) ?? config["SPL_PWD"];

            if (username == null || password == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            else
            {
                var libraryInfo = GetLibraryItemsJson(username, password);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(libraryInfo), Encoding.UTF8, "application/json")
                };
            }
        }

        private static HtmlElement FindByAttribute(HtmlElement root, string attr, string val)
        {
            var findParams = new HtmlElementFindParams();
            findParams.Attributes.Add(attr, val);
            return root.ChildElements.Find(findParams);
        }

        private static IConfigurationRoot GetConfiguration(ExecutionContext context)
        {
            return new ConfigurationBuilder()
                .SetBasePath(context.FunctionAppDirectory)
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        private static LibraryInfo GetLibraryItemsJson(string splUser, string splPwd)
        {
            var basePath = new Uri("https://seattle.bibliocommons.com");
            var page = new HtmlPage(basePath);

            var browserEmulator = new LTAF.Emulators.BrowserEmulator(basePath.AbsoluteUri);
            browserEmulator.SilentMode = true;
            page.BrowserCommandExecutor = browserEmulator.CreateCommandExecutor();

            page.Navigate("/user/login");

            var elements = page.RootElement.ChildElements;
            elements.Find(new { testid = "field_username" }).SetText(splUser);
            elements.Find(new { testid = "field_userpin" }).SetText(splPwd);
            elements.Find(new { testid = "button_login" }).Click();

            page.Navigate("/v2/checkedout");
            var checkedOutItems = SPLHtmlParser.GetCheckedOutItems(page.RootElement, out CheckedOutSummary checkedOutSummary);

            page.Navigate("/v2/holds");
            var heldItems = SPLHtmlParser.GetHeldItems(page.RootElement, out HeldSummary heldSummary);

            var libraryInfo = new LibraryInfo()
            {
                CheckedOutSummary = checkedOutSummary,
                CheckedOutItems = checkedOutItems,
                HeldItems = heldItems,
                HeldSummary = heldSummary
            };

            return libraryInfo;
        }
    }
}