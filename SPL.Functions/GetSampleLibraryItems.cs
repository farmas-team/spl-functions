using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace SPL.Functions
{
    public static class GetSampleLibraryItems
    {
        private static DateTime Today = DateTime.Now;

        [FunctionName("GetSampleLibraryItems")]
        public async static Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await Task.Delay(1000);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JToken.Parse(SampleJson).ToString(), Encoding.UTF8, "application/json")
            };
        }

        private static readonly string EmptyCollectionsJson = @"
{
  'checkedOutSummary': {
    'nextDueDate': null,
    'isExpired': false
  },
  'checkedOutItems': []
}";

        private static readonly string SampleJson = @"
{
  'heldItems': [
    {
      'title': 'interview with God',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317466487/SC.GIF&client=sepup&type=xw12&oclc=&upc=191329078211',
      'metadataId': 'S30C3422748',
      'isReady': true,
      'isInTransit': false,
      'position': 0
    },
    {
      'title': 'Sicario Day of the soldado',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=043396526815',
      'metadataId': 'S30C3401911',
      'isReady': false,
      'isInTransit': true,
      'position': 60
    },
    {
      'title': 'iGen',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9781501152023/SC.GIF&client=sepup&type=xw12&oclc=',
      'metadataId': 'S30C3296944',
      'isReady': false,
      'isInTransit': false,
      'position': 4
    },
    {
      'title': 'MEMOIR OF WAR DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=751778951185',
      'metadataId': 'S30C3422757',
      'isReady': false,
      'isInTransit': false,
      'position': 36
    },
    {
      'title': 'big bang theory The complete eleventh season',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929609062',
      'metadataId': 'S30C3389119',
      'isReady': false,
      'isInTransit': false,
      'position': 49
    },
    {
      'title': 'CAPTAIN DVD',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=751778951208',
      'metadataId': 'S30C3421953',
      'isReady': false,
      'isInTransit': false,
      'position': 63
    },
    {
      'title': 'death of Stalin',
      'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429308405',
      'metadataId': 'S30C3377975',
      'isReady': false,
      'isInTransit': false,
      'position': 63
  }],
  'checkedOutSummary': {
    'nextDueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(5)) + @"',
    'isExpired': false
  },
  'checkedOutItems': [
  {
    'title': 'The Righteous Mind',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9780307907035/SC.GIF&client=sepup&type=xw12&oclc=',
    'barcode': 'mq4502898',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(-1)) + @"',
    'metadataId': 'S30C2819725'
  },
  {
    'title': 'The Big Bang Theory',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929566648',
    'barcode': '0010090550723',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today) + @"',
    'metadataId': 'S30C3282119'
  },
  {
    'title': '5-minute Princess Stories',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9781423146575/SC.GIF&client=sepup&type=xw12&oclc=',
    'barcode': '0010092139319',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(-2)) + @"',
    'metadataId': 'S30C2876756'
  },
  {
    'title': 'Atomic Blonde',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317017986/SC.GIF&client=sepup&type=xw12&oclc=&upc=025192396915',
    'barcode': '0010093117983',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(5)) + @"',
    'metadataId': 'S30C3299221'
  },
  {
    'title': 'The Catcher Was A Spy',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429311979',
    'barcode': '0010096870760',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(10)) + @"',
    'metadataId': 'S30C3404311'
  },
  {
    'title': 'Eighth Grade',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=031398292487',
    'barcode': '0010096091615',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(10)) + @"',
    'metadataId': 'S30C3401880'
  },
  {
    'title': 'The Good Fight',
    'imageSrc': 'https://secure.syndetics.com/index.aspx?isbn=9786317214750/SC.GIF&client=sepup&type=xw12&oclc=&upc=032429305312',
    'barcode': '0010093338167',
    'dueDate': '" + String.Format("{0:yyyy-MM-dd}", Today.AddDays(10)) + @"',
    'metadataId': 'S30C3363386'
  }
]}";
    }
}
