using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPL.Functions.Models
{
    public class LibraryInfo
    {
        [JsonProperty("checkedOutSummary")]
        public CheckedOutSummary CheckedOutSummary { get; set; }

        [JsonProperty("checkedOutItems")]
        public IList<CheckedOutItem> CheckedOutItems { get; set; }

        [JsonProperty("heldItems")]
        public IList<HeldItem> HeldItems { get; set; }

        [JsonProperty("heldSummary")]
        public HeldSummary HeldSummary { get; set; }
    }
}
