﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPL.Functions.Models
{
    public class HeldSummary
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("anyReady")]
        public bool AnyReady { get; set; }
    }
}
