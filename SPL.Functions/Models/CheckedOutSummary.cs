﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPL.Functions.Models
{
    public class CheckedOutSummary
    {
        [JsonProperty("nextDueDate")]
        public string NextDueDate { get; set; }

        [JsonProperty("isExpired")]
        public bool IsExpired { get; set; }
    }
}
