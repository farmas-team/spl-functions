﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPL.Functions.Models
{
    public class HeldItem
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("imageSrc")]
        public string ImageSrc { get; set; }

        [JsonProperty("metadataId")]
        public string MetadataId { get; set; }

        [JsonProperty("isReady")]
        public bool IsReady { get; set; }

        [JsonProperty("isInTransit")]
        public bool IsInTransit { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }
    }
}
