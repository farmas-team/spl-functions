﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPL.Functions.Models
{
    public class CheckedOutItem
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("imageSrc")]
        public string ImageSrc { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("dueDate")]
        public string DueDate { get; set; }

        [JsonProperty("metadataId")]
        public string MetadataId { get; set; }

        [JsonProperty("isExpired")]
        public bool IsExpired { get; set; }
    }
}
