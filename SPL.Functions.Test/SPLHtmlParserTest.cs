﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using SPL.Functions;
using LTAF;
using XUnitAssert = Xunit.Assert;
using SPL.Functions.Models;

namespace SPL.Functions.Test
{
    public class SPLHtmlParserTest
    {
        [Fact]
        public void HeldItems_OneReadyOneInTransit()
        {
            var html = File.ReadAllText("SampleResponses\\ManyHoldsOneReadyOneInTransit.txt");

            var page = HtmlElement.Create(html);
            var items = SPLHtmlParser.GetHeldItems(page, out HeldSummary summary);

            XUnitAssert.True(summary.AnyReady);
            XUnitAssert.Equal(35, summary.Total);
            XUnitAssert.Equal(25, items.Count);

            var first = items.First();
            XUnitAssert.Equal("interview with God", first.Title);
            XUnitAssert.True(first.IsReady);
            XUnitAssert.False(first.IsInTransit);
            XUnitAssert.NotNull(first.ImageSrc);

            var second = items.Skip(1).First();
            XUnitAssert.Equal("Sicario Day of the soldado", second.Title);
            XUnitAssert.False(second.IsReady);
            XUnitAssert.True(second.IsInTransit);

            var pending = items.Skip(2);
            XUnitAssert.True(pending.All(i => !i.IsReady && !i.IsInTransit));
        }

        [Fact]
        public void NoCheckedOutItems()
        {
            var html = File.ReadAllText("SampleResponses\\NoCheckedOut.txt");

            var page = HtmlElement.Create(html);
            var items = SPLHtmlParser.GetCheckedOutItems(page, out CheckedOutSummary summary);

            XUnitAssert.Empty(items);
            XUnitAssert.False(summary.IsExpired);
            XUnitAssert.Null(summary.NextDueDate);
        }

        [Fact]
        public void CheckedOutSummary_OneOverdue()
        {
            var html = File.ReadAllText("SampleResponses\\ManyCheckedoutOneOverdue.txt");

            var page = HtmlElement.Create(html);
            SPLHtmlParser.GetCheckedOutItems(page, out CheckedOutSummary summary);

            XUnitAssert.Equal("2018-12-13", summary.NextDueDate);
            XUnitAssert.True(summary.IsExpired);
        }

        [Fact]
        public void CheckedOutSummary_NoneDue()
        {
            var html = File.ReadAllText("SampleResponses\\ManyCheckedoutNoneDue.txt");

            var page = HtmlElement.Create(html);
            SPLHtmlParser.GetCheckedOutItems(page, out CheckedOutSummary summary);

            XUnitAssert.Equal("2018-12-13", summary.NextDueDate);
            XUnitAssert.False(summary.IsExpired);
        }

        [Fact]
        public void CheckedOutItems_ThreeCheckedOut()
        {
            var html = File.ReadAllText("SampleResponses\\ManyCheckedoutNoneDue.txt");

            var page = HtmlElement.Create(html);
            var items = SPLHtmlParser.GetCheckedOutItems(page, out CheckedOutSummary summary);

            XUnitAssert.NotEmpty(items);
            XUnitAssert.Equal(3, items.Count);

            var item = items.First();
            XUnitAssert.Equal("The Big Bang Theory", item.Title);
            XUnitAssert.Equal("2018-12-13", item.DueDate);
            XUnitAssert.Equal("https://secure.syndetics.com/index.aspx?isbn=/SC.GIF&client=sepup&type=xw12&oclc=&upc=883929566648", item.ImageSrc);
        }

        [Fact]
        public void CheckedOutItems_SevenCheckedOut()
        {
            var html = File.ReadAllText("SampleResponses\\ManyCheckedoutMultipleTypes.txt");

            var page = HtmlElement.Create(html);
            var items = SPLHtmlParser.GetCheckedOutItems(page, out CheckedOutSummary summary);

            XUnitAssert.NotEmpty(items);
            XUnitAssert.Equal(7, items.Count);
        }
    }
}
