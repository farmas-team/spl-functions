# SPL Functions

This project is the back-end for the [Seattle Public Library Chrome Extension](https://bitbucket.org/farmas-team/spl-chrome-extension).

## Deployment

The project is designed to be hosted as Azure Functions. To setup your own instance:
1. Fork this repository.
2. Create a new Azure Functions App.
3. Hook up your new repository to the function app.
4. Trigger a build.

This will create 2 azure functions in the app:
- GetSampleLibraryItems. Used to return fake data.
- GetLibraryStatus. Logs in to your library account and retrieves the status of your collections.

### Test Deployment

You can set the environment variables SPL_USER and SPL_PWD to your own account to test that the function successfully returns the items for your account.

## Local Development

To run this application in development mode:

1. Install the Azure Functions CLI.
2. Build the project with 'dotnet build'.
3. Go to the output directory and run 'func host start --cors *'

